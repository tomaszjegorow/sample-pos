package pl.moose.test.simplepos.builder;

import pl.moose.test.simplepos.model.Product;

/**
 * Builder dla {@link Product}
 */
public class ProductBuilder {
	private String barcode;
	private String name;
	private int price;

	public ProductBuilder withBarcode(String barcode) {
		this.barcode = barcode;
		return this;
	}

	public ProductBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public ProductBuilder withPrice(int price) {
		this.price = price;
		return this;
	}

	public Product build() {
		Product product = new Product();
		product.setBarcode(barcode);
		product.setName(name);
		product.setPrice(price);
		return product;
	}
}
