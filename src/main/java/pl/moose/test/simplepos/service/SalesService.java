package pl.moose.test.simplepos.service;

import pl.moose.test.simplepos.BaseService;
import pl.moose.test.simplepos.dto.SaleReceiptDto;
import pl.moose.test.simplepos.exception.ProductNotFoundException;
import pl.moose.test.simplepos.exception.SaleNotFoundException;
import pl.moose.test.simplepos.model.Product;

/**
 * Interfejs serwisu do zarządzania sprzedażą. Zarządza stanem różnych transakcji sprzedaży.
 */
public interface SalesService extends BaseService {
	/**
	 * Dla danej sesji sprzedaży rejestruje produkt o danym kodzie kreskowym
	 *
	 * @param sessionId ID sesji sprzedaży
	 * @param barcode kod kreskowy
	 *
	 * @throws ProductNotFoundException jeśli nie odnaleziono produktu o danym kodzie kreskowym
	 */
	Product registerProductForSale(String sessionId, String barcode) throws ProductNotFoundException;

	/**
	 * Pobiera rachunek dla danej sesji sprzedaży
	 *
	 * @param sessionId ID sesji sprzedaży
	 *
	 * @return rachunek
	 */
	SaleReceiptDto getSaleReceipt(String sessionId) throws SaleNotFoundException;

	/**
	 * Usuwa sesję sprzedaży o danym ID
	 *
	 * @param sessionId ID sesji sprzedaży
	 */
	void removeSale(String sessionId);
}
