package pl.moose.test.simplepos.service.impl;

import pl.moose.test.simplepos.dto.SaleReceiptDto;
import pl.moose.test.simplepos.dto.SaleReceiptPositionDto;
import pl.moose.test.simplepos.service.PrinterService;
import pl.moose.test.simplepos.util.StringUtils;

/**
 * Implementacja serwisu do wydruków
 */
public class PrinterServiceImpl implements PrinterService {
	@Override
	public void init() {
		// do nothing
	}

	@Override
	public void printSaleReceipt(SaleReceiptDto saleReceiptDto) {
		// Symulacja wykorzystania API do wysłania komunikatu do wyświetlacza LCD

		String printStr = "";
		for (SaleReceiptPositionDto saleReceiptPositionDto : saleReceiptDto.getPositions()) {
			printStr += String.format("%s: %dx%s\t%s\n",//
					saleReceiptPositionDto.getProductName(),//
					saleReceiptPositionDto.getCount(),//
					StringUtils.priceToPln(saleReceiptPositionDto.getProductPrice()),//
					StringUtils.priceToPln(saleReceiptPositionDto.getSumCost())//
			);
		}
		printStr += String.format("SUMA: %s", StringUtils.priceToPln(saleReceiptDto.getCostSum()));

		System.out.println(">>\n[PRINTER]\n" + printStr + "\n<<\n");
	}
}
