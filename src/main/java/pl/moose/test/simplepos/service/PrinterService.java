package pl.moose.test.simplepos.service;

import pl.moose.test.simplepos.BaseService;
import pl.moose.test.simplepos.dto.SaleReceiptDto;

/**
 * Interfejs serwisu do wydruków
 */
public interface PrinterService extends BaseService {
	/**
	 * Drukuje rachunek sprzedaży
	 *
	 * @param saleReceiptDto DTO z informacją o rachunku sprzedaży
	 */
	void printSaleReceipt(SaleReceiptDto saleReceiptDto);
}
