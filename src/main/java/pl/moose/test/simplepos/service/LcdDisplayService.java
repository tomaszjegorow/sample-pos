package pl.moose.test.simplepos.service;

import pl.moose.test.simplepos.BaseService;

/**
 * Interfejs serwisu do komunikacji z wyświetlaczem LCD
 */
public interface LcdDisplayService extends BaseService {
	void printMessage(String message);
}
