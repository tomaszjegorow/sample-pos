package pl.moose.test.simplepos.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.moose.test.simplepos.ServiceManager;
import pl.moose.test.simplepos.dao.ProductDao;
import pl.moose.test.simplepos.dto.SaleReceiptDto;
import pl.moose.test.simplepos.dto.SaleReceiptPositionDto;
import pl.moose.test.simplepos.exception.ProductNotFoundException;
import pl.moose.test.simplepos.exception.SaleNotFoundException;
import pl.moose.test.simplepos.model.Product;
import pl.moose.test.simplepos.service.SalesService;

/**
 * Implementacja serwisu do zarządzania sprzedażą. Zarządza stanem różnych transakcji sprzedaży.
 */
public class SalesServiceImpl implements SalesService {
	private ProductDao productDao;

	/**
	 * Mapa sesji sprzedaży
	 */
	private Map<String, Sale> sessions = Collections.synchronizedMap(new HashMap<String, Sale>());

	@Override
	public void init() {
		productDao = ServiceManager.get().getProductDao();
	}

	@Override
	public Product registerProductForSale(String sessionId, String barcode) throws ProductNotFoundException {
		// jeśli nie ma jeszcze sesji sprzedaży -- tworzymy ją
		if (!sessions.containsKey(sessionId)) {
			sessions.put(sessionId, new Sale());
		}

		Product product = productDao.getProductByBarcode(barcode);
		sessions.get(sessionId).addProduct(product);

		return product;
	}

	@Override
	public SaleReceiptDto getSaleReceipt(String sessionId) throws SaleNotFoundException {
		Sale sale = sessions.get(sessionId);
		if (sale == null) {
			throw new SaleNotFoundException();
		}

		int costSum = 0;
		List<SaleReceiptPositionDto> saleReceiptPositions = new ArrayList<SaleReceiptPositionDto>();
		for (SalePosition salePosition : sale.positions.values()) {
			costSum += salePosition.productPrice * salePosition.count;
			saleReceiptPositions.add(new SaleReceiptPositionDto(//
					salePosition.productName,//
					salePosition.productPrice,//
					salePosition.count//
			));
		}

		return new SaleReceiptDto(costSum, saleReceiptPositions);
	}

	@Override
	public void removeSale(String sessionId) {
		sessions.remove(sessionId);
	}

	/**
	 * Reprezentuje stan sprzedaży
	 */
	private static class Sale {
		private final Map<String, SalePosition> positions = new HashMap<String, SalePosition>();

		public void addProduct(Product product) {
			if (positions.containsKey(product.getBarcode())) {
				positions.get(product.getBarcode()).count++;
			}
			else {
				SalePosition salePosition = new SalePosition();
				salePosition.productName = product.getName();
				salePosition.productPrice = product.getPrice();
				salePosition.count = 1;
				positions.put(product.getBarcode(), salePosition);
			}
		}
	}

	/**
	 * Pozycja sprzedaży
	 */
	private static class SalePosition {
		private String productName;
		private int productPrice = 0;
		private int count = 0;
	}
}
