package pl.moose.test.simplepos.service.impl;

import pl.moose.test.simplepos.service.LcdDisplayService;

/**
 * Implementacja serwisu do komunikacji z wyświetlaczem LCD
 */
public class LcdDisplayServiceImpl implements LcdDisplayService {
	@Override
	public void init() {
		// do nothing
	}

	@Override
	public void printMessage(String message) {
		// Symulacja wykorzystania API do wysłania komunikatu do wyświetlacza LCD
		System.out.println(">>\n[LCD]\n" + message + "\n<<\n");
	}
}
