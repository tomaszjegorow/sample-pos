package pl.moose.test.simplepos.handler.impl;

import pl.moose.test.simplepos.ServiceManager;
import pl.moose.test.simplepos.dto.SaleReceiptDto;
import pl.moose.test.simplepos.exception.ProductNotFoundException;
import pl.moose.test.simplepos.exception.SaleNotFoundException;
import pl.moose.test.simplepos.handler.BarcodeScannerHandler;
import pl.moose.test.simplepos.model.Product;
import pl.moose.test.simplepos.service.LcdDisplayService;
import pl.moose.test.simplepos.service.PrinterService;
import pl.moose.test.simplepos.service.SalesService;
import pl.moose.test.simplepos.util.StringUtils;

/**
 * Implementacja handlera na zdarzenie otrzymania kodu kreskowego ze skanera
 */
public class BarcodeScannerHandlerImpl implements BarcodeScannerHandler {
	public static final String FINISH_TRANSACTION_CMD = "exit";

	private SalesService salesService;
	private LcdDisplayService lcdDisplayService;
	private PrinterService printerService;

	@Override
	public void init() {
		salesService = ServiceManager.get().getSalesService();
		lcdDisplayService = ServiceManager.get().getLcdDisplayService();
		printerService = ServiceManager.get().getPrinterService();
	}

	@Override
	public void doHandle(String sessionId, String barcode) {
		try {
			if (StringUtils.isBlank(barcode)) {
				lcdDisplayService.printMessage("Invalid bar-code");
			}
			else if (FINISH_TRANSACTION_CMD.equals(barcode)) {
				SaleReceiptDto saleReceiptDto = salesService.getSaleReceipt(sessionId);
				lcdDisplayService.printMessage(String.format("%s", StringUtils.priceToPln(saleReceiptDto.getCostSum())));
				printerService.printSaleReceipt(saleReceiptDto);
				salesService.removeSale(sessionId);
			}
			else {
				Product product = salesService.registerProductForSale(sessionId, barcode);
				lcdDisplayService
						.printMessage(String.format("%s, %s", product.getName(), StringUtils.priceToPln(product.getPrice())));
			}
		}
		catch (SaleNotFoundException e) {
			System.err.append(String.format("Nie odnaleziono sesji sprzedaży o ID='%s'", sessionId));
		}
		catch (ProductNotFoundException e) {
			lcdDisplayService.printMessage("Product not found");
		}
	}
}
