package pl.moose.test.simplepos.handler;

import pl.moose.test.simplepos.BaseService;

/**
 * Intrefejs handlera na zdarzenie otrzymania kodu kreskowego ze skanera
 */
public interface BarcodeScannerHandler extends BaseService {
	/**
	 * Obsługa zdarzenia otrzymania kodu kreskowego
	 *
	 * @param sessionId ID sesji
	 * @param barcode kod kreskowy
	 */
	void doHandle(String sessionId, String barcode);
}
