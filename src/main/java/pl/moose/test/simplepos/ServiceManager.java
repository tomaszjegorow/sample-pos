package pl.moose.test.simplepos;

import pl.moose.test.simplepos.dao.ProductDao;
import pl.moose.test.simplepos.dao.impl.ProductDaoImpl;
import pl.moose.test.simplepos.handler.BarcodeScannerHandler;
import pl.moose.test.simplepos.handler.impl.BarcodeScannerHandlerImpl;
import pl.moose.test.simplepos.service.LcdDisplayService;
import pl.moose.test.simplepos.service.PrinterService;
import pl.moose.test.simplepos.service.SalesService;
import pl.moose.test.simplepos.service.impl.LcdDisplayServiceImpl;
import pl.moose.test.simplepos.service.impl.PrinterServiceImpl;
import pl.moose.test.simplepos.service.impl.SalesServiceImpl;

/**
 * Fabryka/zarządca serwisów (do symulacji Dependency Injection)
 */
public class ServiceManager {
	private static volatile ServiceManager INSTANCE = null;

	private ProductDao productDao;
	private SalesService salesService;
	private BarcodeScannerHandler barcodeScannerHandler;
	private PrinterService printerService;
	private LcdDisplayService lcdDisplayService;

	private ServiceManager() {
	}

	public static synchronized ServiceManager get() {
		if (INSTANCE == null) {
			INSTANCE = new ServiceManager();
			INSTANCE.init();
		}
		return INSTANCE;
	}

	private void init() {
		// create
		productDao = new ProductDaoImpl();
		salesService = new SalesServiceImpl();
		barcodeScannerHandler = new BarcodeScannerHandlerImpl();
		printerService = new PrinterServiceImpl();
		lcdDisplayService = new LcdDisplayServiceImpl();

		// init
		productDao.init();
		salesService.init();
		barcodeScannerHandler.init();
		printerService.init();
		lcdDisplayService.init();
	}

	public ProductDao getProductDao() {
		return productDao;
	}

	public SalesService getSalesService() {
		return salesService;
	}

	public BarcodeScannerHandler getBarcodeScannerHandler() {
		return barcodeScannerHandler;
	}

	public PrinterService getPrinterService() {
		return printerService;
	}

	public LcdDisplayService getLcdDisplayService() {
		return lcdDisplayService;
	}
}
