package pl.moose.test.simplepos.util;

/**
 * Util do stringów
 */
public class StringUtils {
	/**
	 * Sprawdza czy string jest pusty
	 *
	 * @param str string
	 *
	 * @return true/false
	 */
	public static boolean isBlank(String str) {
		return str == null || str.trim().isEmpty();
	}

	/**
	 * Konwertuje koszt do stringa
	 *
	 * @param price koszt w groszach
	 *
	 * @return koszt w string
	 */
	public static String priceToPln(int price) {
		return String.format("%szł", ((double) price / 100));
	}
}
