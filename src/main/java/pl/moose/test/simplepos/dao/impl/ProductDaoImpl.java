package pl.moose.test.simplepos.dao.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import pl.moose.test.simplepos.builder.ProductBuilder;
import pl.moose.test.simplepos.dao.ProductDao;
import pl.moose.test.simplepos.exception.ProductNotFoundException;
import pl.moose.test.simplepos.model.Product;

/**
 * Implementacja DAO dla {@link Product}
 */
public class ProductDaoImpl implements ProductDao {
	/**
	 * Symulacja bazy danych z produktami
	 */
	private static final Map<String, Product> PROD_DB = Collections.synchronizedMap(new HashMap<String, Product>());

	static {
		Product sliwkiProduct = new ProductBuilder()//
				.withBarcode("sliwki123")//
				.withName("Śliwki")//
				.withPrice(799)//
				.build();
		PROD_DB.put(sliwkiProduct.getBarcode(), sliwkiProduct);

		Product serProduct = new ProductBuilder()//
				.withBarcode("ser123")//
				.withName("Ser Żółty")//
				.withPrice(2499)//
				.build();
		PROD_DB.put(serProduct.getBarcode(), serProduct);

		Product piwoProduct = new ProductBuilder()//
				.withBarcode("piwo123")//
				.withName("Piwo")//
				.withPrice(399)//
				.build();
		PROD_DB.put(piwoProduct.getBarcode(), piwoProduct);

		Product gazetaProduct = new ProductBuilder()//
				.withBarcode("gazeta123")//
				.withName("Gazeta")//
				.withPrice(399)//
				.build();
		PROD_DB.put(gazetaProduct.getBarcode(), gazetaProduct);
	}

	@Override
	public void init() {
		// do nothing
	}

	@Override
	public Product getProductByBarcode(String barcode) throws ProductNotFoundException {
		Product product = PROD_DB.get(barcode);
		if (product != null) {
			return product;
		}
		throw new ProductNotFoundException();
	}
}
