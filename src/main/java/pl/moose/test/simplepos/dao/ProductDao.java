package pl.moose.test.simplepos.dao;

import pl.moose.test.simplepos.BaseService;
import pl.moose.test.simplepos.exception.ProductNotFoundException;
import pl.moose.test.simplepos.model.Product;

/**
 * Interfejs DAO dla {@link Product}
 */
public interface ProductDao extends BaseService {
	/**
	 * Pobiera produkt dla danego kodu kreskowego
	 *
	 * @param barcode kod kreskowy
	 *
	 * @return produkt
	 */
	Product getProductByBarcode(String barcode) throws ProductNotFoundException;
}
