package pl.moose.test.simplepos.exception;

/**
 * Wyjątek ciskany, gdy nie odnaleziono danego produktu
 */
public class ProductNotFoundException extends Exception {
}
