package pl.moose.test.simplepos.exception;

/**
 * Wyjątek ciskany, gdy nie odnaleziono danej sprzedaży
 */
public class SaleNotFoundException extends Exception {
}
