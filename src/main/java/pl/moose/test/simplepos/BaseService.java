package pl.moose.test.simplepos;

/**
 * Klasa bazowa dla serwisów zarządzanych przez {@link ServiceManager}
 */
public interface BaseService {
	/**
	 * Inicjalizuje serwis
	 */
	void init();
}
