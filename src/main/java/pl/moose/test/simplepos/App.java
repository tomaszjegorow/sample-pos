package pl.moose.test.simplepos;

import java.util.Scanner;
import pl.moose.test.simplepos.handler.BarcodeScannerHandler;

/**
 * Główna pętla aplikacji
 */
public class App {
	private static final String BARCODE_SCANNER_ID = "someBarcodeScannerId";

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		BarcodeScannerHandler barcodeScannerHandler = ServiceManager.get().getBarcodeScannerHandler();

		do {
			// symulujemy skanowanie kodu przez skaner
			System.out.print("INPUT: ");
			String line = scanner.nextLine();
			barcodeScannerHandler.doHandle(BARCODE_SCANNER_ID, line);
		} while (scanner.hasNextLine());
	}
}
