package pl.moose.test.simplepos.dto;

/**
 * Pozycja rachunku za sprzedaż
 */
public class SaleReceiptPositionDto {
	private final String productName;
	private final int productPrice;
	private final int count;

	public SaleReceiptPositionDto(String productName, int productPrice, int count) {
		this.productName = productName;
		this.productPrice = productPrice;
		this.count = count;
	}

	public String getProductName() {
		return productName;
	}

	public int getProductPrice() {
		return productPrice;
	}

	public int getCount() {
		return count;
	}

	// pomocnicze

	public int getSumCost() {
		return count * productPrice;
	}
}
