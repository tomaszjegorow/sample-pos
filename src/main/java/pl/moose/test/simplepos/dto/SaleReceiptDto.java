package pl.moose.test.simplepos.dto;

import java.util.List;

/**
 * Rachunek za sprzedaż
 */
public class SaleReceiptDto {
	private final List<SaleReceiptPositionDto> positions;
	private final int costSum;

	public SaleReceiptDto(int costSum, List<SaleReceiptPositionDto> positions) {
		this.costSum = costSum;
		this.positions = positions;
	}

	public List<SaleReceiptPositionDto> getPositions() {
		return positions;
	}

	public int getCostSum() {
		return costSum;
	}
}
